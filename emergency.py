
from datetime import datetime, timedelta, date

from trytond.model import ModelView, ModelSQL, fields, Unique, Workflow
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Or
from trytond.report import Report
from trytond.wizard import Wizard, StateTransition
from trytond.i18n import gettext
from trytond.exceptions import UserError

TRIAGE_LEVEL = [
    ('', ''),
    ('level1', 'Level I'),
    ('level2', 'Level II'),
    ('level3', 'Level III'),
    ('level4', 'Level IV'),
    ('level5', 'Level V'),
]

YES_NO = [
    (None, ''),
    ('yes', 'Yes'),
    ('no', 'No'),
    ]

STATUS = [
    ('', ''),
    ('conscious', 'Conscious'),
    ('unconscious', 'Unconscious'),
    ('coma', 'Coma'),
    ('dead', 'Dead'),
]

TRANSFER_ROOM = [
    ('', ''),
    ('general_operatinpg_room', 'General Operating Room'),
    ('emergency_operating_room', 'Emergency Operating Room'),
    ('observation', 'Observation'),
    ('triaje1', 'Triaje 1'),
    ('triaje2', 'Triaje 2'),
    ('pre-emergency', 'Pre Emergency'),
    ('curettage', 'Curettage IV'),
    ('external referral', 'External Referral'),
]

PAIN_SCALE = [
    ('', ''),
    ('without pain', 'Without Pain'),
    ('slight pain', 'Slight Pain'),
    ('moderate pain', 'Moderate Pain'),
    ('severe pain', 'Severe Pain'),
    ('very severe pain', 'Very Severe Pain'),
    ('maximum pain', 'Maximum Pain'),
]

TOXIC_HABITS = [
    ('', ''),
    ('coffee', 'Coffee'),
    ('cigarette', 'Cigarette'),
    ('tobacco', 'Tobacco'),
    ('pipa', 'Pipa'),
    ('alcohol', 'Alcohol'),
    ('drugs', 'Drugs'),
]

DISCHARGE_DESTINATION = [
    ('', ''),
    ('house', 'House'),
    ('hospitalization', 'Hospitalization'),
    ('transfer to general operating room', 'Transfer to general operating room'),
    ('transfer to other center', 'Transfer to other center'),
    ('resuscitation room', 'Resuscitation room'),
    ('rehydration room', 'Rehydration room'),
    ('post-emergency room', 'Post-emergency room'),
    ('curettage room', 'Curettage room'),
    ('nebulization room', 'Nebulization room'),
    ('external referral', 'External referral'),
    ('morgue', 'Morgue'),
    ('leakage', 'Leakage'),
    ('consultation', 'Consultation'),
]


class Emergency(Workflow, ModelSQL, ModelView):
    'Emergency'
    __name__ = 'health.emergency'
    _rec_name = 'number'
    STATES_ADMISSION = {'readonly': Eval('state') != 'draft'}
    # For diagnosis
    STATES = {'readonly': Eval('state') != 'diagnosis'}
    STATES_REFERRED = {
        'required': Bool(Eval('referred_patient')),
        'invisible': ~Bool(Eval('referred_patient'))
    }

    number = fields.Char('Number', readonly=True, select=True)
    puid = fields.Function(fields.Char('PUID'), 'get_puid',
        searcher='search_puid')
    patient = fields.Many2One('health.patient', 'Patient',
        required=True, select=True, states=STATES_ADMISSION)
    registration_date = fields.DateTime('Registration date',
        required=True, select=True, states={
            'readonly': True,
        })
    location = fields.Many2One('stock.location', 'Location',
        states={
            'readonly': Eval('state').in_(['discharged', 'cancelled'])
        }, domain=[(
            'type', '=', 'warehouse'
        )])
    # internal_stock_requests = fields.One2Many('stock.shipment.internal',
    #     'emergency', 'Internal Stock Requests', states=STATES)
    insurance_plan = fields.Many2One('health.insurance.plan',
        'Insurance Plan', states=STATES_ADMISSION)
    admission_type = fields.Selection([
        (None, ''),
        # ('routine', 'Routine'),
        # ('maternity', 'Maternity'),
        # ('elective', 'Elective'),
        # ('urgent', 'Urgent'),
        ('emergency', 'Emergency'),
        ], 'Admission type', required=True, states=STATES_ADMISSION)
    attending_medical_prof = fields.Many2One('health.professional',
        'Attending Medical Prof.',  states={'readonly': True})
    admission_reason = fields.Many2One('health.pathology',
        'Reason for Admission', help="Reason for Admission", select=True,
        states=STATES_ADMISSION)
    presumptive_diagnosis = fields.Char('Presumptive Diagnosis',
        states=STATES)
    bed = fields.Many2One('health.hospital.bed', 'Hospital Bed',
        states={
          'readonly': Or(
                Eval('state') == 'admitted',
                Bool(Eval('number')),
            )}, depends=['number'])
    state = fields.Selection([
        (None, ''),
        ('draft', 'Draft'),
        ('triage', 'Triage'),
        ('cancelled', 'Cancelled'),
        ('admitted', 'Admitted'),
        ('diagnosis', 'Diagnosis'),
        ('discharged', 'Discharged'),
        ], 'Status', select=True, readonly=True)
    discharged_by = fields.Many2One(
        'health.professional', 'Discharged by', readonly=True,
        help="Health Professional that discharged the patient")
    institution = fields.Many2One('health.institution', 'Institution',
        readonly=True)
    referred_by = fields.Many2One('health.institution', 'Reffered By',
        states=STATES)
    triage_level = fields.Selection(TRIAGE_LEVEL, 'Triage Level',
        states=STATES_ADMISSION)
    triage_level_string = triage_level.translated('triage_level')
    pain_scale = fields.Selection(PAIN_SCALE, 'Pain Scale',
        states=STATES)
    pain_scale_string = pain_scale.translated('pain_scale')
    transfer_rooms = fields.Selection(TRANSFER_ROOM, 'Transfer Room',
        states=STATES)
    transfer_rooms_string = transfer_rooms.translated('transfer_rooms')
    vital_signs = fields.One2Many('health.emergency.vital_signs',
        'emergency', 'Vital Signs', states=STATES)
    arrival_status = fields.Selection(STATUS, 'Arrival Status',
        states=STATES)
    arrival_status_string = arrival_status.translated('arrival_status')
    emergency_detail = fields.Char('Emergency Detail', states=STATES)
    #Antecedents / Toxic Habits
    disease_history = fields.Text('Disease History', states=STATES)
    childhood_history = fields.Text('Childhood', states=STATES)
    adolescence_history = fields.Text('Adolescence', states=STATES)
    adulthood_history = fields.Text('Adulthood', states=STATES)
    family_history = fields.Text('Family History', states=STATES)
    obstetrics_gynecology = fields.Boolean('Obstetrics Gynecology',
        states=STATES)
    date_last_menstruation = fields.Date('DLM', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    }, help='Date of last menstruation')
    births = fields.Integer('Births', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    abortions = fields.Integer('Abortions', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    caesarean = fields.Integer('Caesarean', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    pregnancies = fields.Integer('Pregnancies', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology')),
    })
    t_gestations = fields.Integer('T. Gestations', states={
        'invisible': ~Bool(Eval('obstetrics_gynecology'))
    })
    common_medication = fields.Text('Common Medication', states=STATES)
    blood_transfusion = fields.Text('Blood Transfusion', states=STATES)
    #toxic_habits = fields.One2Many('health.emergency.toxic_habits',
        #'emergency', 'Toxic Habits', states=STATES)
    coffee = fields.Selection(YES_NO, 'Coffee', states=STATES)
    coffee_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('coffee', '') != 'yes')
    })
    smoke = fields.Selection(YES_NO, 'Smoke', states=STATES)
    smoke_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('smoke', '') != 'yes')
    })
    alcohol = fields.Selection(YES_NO, 'Alcohol', states=STATES)
    alcohol_frequency = fields.Char('Frequency', states={
        'invisible': Bool(Eval('alcohol', '') != 'yes')
    })
    other_habits = fields.Char('Other Habits', help="For example drugs consume", states=STATES)
    medications_allergy = fields.Text('Medications Allegy', states=STATES)
    food_allergy = fields.Text('Food Allegy', states=STATES)
    other_allergy = fields.Text('Other Allegy', states=STATES)
    surgical_history = fields.Text('Surgical', states=STATES)
    trauma_history = fields.Text('Trauma', states=STATES)
    # Third Page Physical State
    physical_state = fields.Text('Physical', states=STATES)
    neurological_state = fields.Text('Neurological', states=STATES)
    head_neck_state = fields.Text('Head Neck', states=STATES)
    thorax_state = fields.Text('Thorax', states=STATES)
    heart_state = fields.Text('Heart', states=STATES)
    lungs_state = fields.Text('Lungs', states=STATES)
    abdomen_state = fields.Text('Abdomen', states=STATES)
    pelvis_state = fields.Text('Pelvis', states=STATES)
    vaginal_examination = fields.Text('Vaginal Examination', states=STATES)
    extremities_state = fields.Text('Extremities', states=STATES)
    other_state = fields.Text('Other State', states=STATES)
    disability = fields.Boolean('Disability', states=STATES)
    disability_description = fields.Text('Disability Description', states={
        'required': Bool(Eval('disability')),
        'invisible': ~Bool(Eval('disability'))
    })
    # Evaluations and interconsultation
    evaluations = fields.One2Many('health.emergency.note_list',
        'emergency', 'Evaluations', states=STATES)
    interconsultations = fields.One2Many('health.interconsultation',
        'origin', 'Interconsultations', states=STATES)
    # Page Medications / Labs / Images
    medications = fields.One2Many('health.emergency.medication',
        'emergency', 'Medications', states=STATES)
    labs = fields.One2Many('health.lab_test', 'origin',
        'Tests Lab.', states=STATES, context={
            'patient': Eval('patient', -1),
        }, domain=[
            ('patient', '=', Eval('patient')),
            ('priority', '=', 'urgent'),
        ])
    imaging_requests = fields.One2Many('health.imaging.request', 'origin',
        'Images Request', states=STATES, domain=[
            ('patient', '=', Eval('patient')),
            ('urgent', '=', True),
        ], context={
            'patient': Eval('patient', -1),
        })
    #internal_stock_requests = fields.One2Many('stock.shipment.internal', 'origin',
        #'Internal Stock Requests')
    # Sixth Page Medications / Labs
    procedures = fields.One2Many('health.emergency.procedures',
        'emergency', 'Procedures', states=STATES)
    admission_cause = fields.Many2One('health.admission_cause',
        'Admission Cause', states=STATES_ADMISSION)
    ward = fields.Many2One('health.hospital.ward', 'Ward', states=STATES)
    diagnosis_1 = fields.Many2One('health.pathology', 'Diagnosis 1',
        states={
            'readonly': Eval('state').in_(['discharged', 'cancelled']),
            'required': Eval('state') == 'discharged',
        }, depends=['state'])
    diagnosis_2 = fields.Many2One('health.pathology', 'Diagnosis 2',
        states=STATES)
    diagnosis_3 = fields.Many2One('health.pathology', 'Diagnosis 3',
        states=STATES)
    discharge_condition = fields.Char('Discharge Condition', states=STATES)
    state_discharge = fields.Selection(STATUS, 'State Discharge', states=STATES)
    state_discharge_string = state_discharge.translated('state_discharge')
    discharge_destination = fields.Selection(DISCHARGE_DESTINATION,
        'Discharge Destination', depends=['state'], states={
            'readonly': Eval('state').in_(['discharged', 'cancelled']),
            'required': Eval('state') == 'discharged',
        })
    discharge_destination_string = discharge_destination.translated(
        'discharge_destination')
    discharge_attending_physician = fields.Many2One(
        'health.professional', 'Discharge Attending Physician',
        states=STATES)
    medic_team = fields.One2Many('health.emergency.medic_team',
        'emergency', 'Medic Team', states=STATES)
    referred_patient = fields.Boolean('Referred Patient', states=STATES)
    referred_diagnostic = fields.Char('Referred Diagnostic',
        states=STATES_REFERRED)
    information_referred_diagnostic = fields.Char('Who Referred Diagnostic',
        states=STATES_REFERRED)
    position = fields.Char('Position', states=STATES_REFERRED)
    receiving_center = fields.Char('Receivin Center', states=STATES_REFERRED)
    information_authorized_reception = fields.Char('Who autorized The reception',
        states=STATES_REFERRED)
    position_autorized = fields.Char('Position Autorized',
        states=STATES_REFERRED)
    duration_referred_center = fields.Char('Time In Center Referred',
        states=STATES_REFERRED)
    condition_of_referred = fields.Text('Condition Determined The referred',
        states=STATES_REFERRED)
    where_referred_ceas = fields.Many2One('health.institution',
        'Where Referred', states=STATES_REFERRED)
    discharge_date = fields.Date('Discharge Date')
    petition_release = fields.Boolean('Petition Release', states={
        'readonly': Or(
           Eval('state') == 'admitted',
           Bool(Eval('petition_party')),
           Bool(Eval('petition_patient'))
        )
    })
    petition_patient = fields.Boolean('Petition of Patient', states={
        'required': Bool(Eval('petition_release')),
        'invisible': ~Bool(Eval('petition_release')),
        'readonly': Bool(Eval('petition_party'))
    })
    petition_party = fields.Boolean('Petition of Party', states={
        'required': Bool(Eval('petition_release')),
        'invisible': ~Bool(Eval('petition_release')),
        'readonly': Bool(Eval('petition_patient'))
    })
    name_petition_party = fields.Char('Name of party', states={
        'required': Bool(Eval('petition_party')),
        'invisible': ~Bool(Eval('petition_party'))
    })
    document_petition_party = fields.Char('Number Id of Party', states={
        'required': Bool(Eval('petition_party')),
        'invisible': ~Bool(Eval('petition_party'))
    })
    address_petition_party = fields.Char('Adress of Party', states={
        'required': Bool(Eval('petition_party')),
        'invisible': ~Bool(Eval('petition_party'))
    })
    health_service = fields.Many2One('health.service', 'Health Service',
        help="Relation to health service created", select=True,
        states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(Emergency, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('name_unique', Unique(t, t.number),
                'The Emergency number already exists'),
        ]
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'triage',
            },
            'triage': {
                'invisible': Eval('state') != 'draft',
            },
            'admit': {
                'invisible': Eval('state') != 'triage'
            },
            'diagnosis': {
                'invisible': Eval('state') != 'admitted'
            },
            'cancel': {
                'invisible': Eval('state') != 'admitted',
            },
            'discharge': {
                'invisible': Eval('state') != 'diagnosis',
            },
        })
        cls._transitions |= set((
            ('draft', 'triage'),
            ('triage', 'admitted'),
            ('admitted', 'diagnosis'),
            ('diagnosis', 'discharged'),
            ('admitted', 'cancelled'),
            ('admitted', 'triage'),
            ('cancelled', 'draft'),
            ('triage', 'draft'),
        ))

    def get_puid(self, name):
        return self.patient.puid

    def set_number(self):
        if self.number:
            return
        config = Pool().get('health.configuration').get_config()
        number = None
        if config and config.emergency_sequence:
            number = config.emergency_sequence.get()
        else:
            raise UserError(gettext(
                'health_emergency.msg_emergency_sequence_missing'
            ))
        self.write([self], {'number': number})

    @classmethod
    def search_puid(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('patient.puid', clause[1], value))
        return res

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('triage')
    def triage(cls, records):
        for record in records:
            record.get_antecedents()

    @classmethod
    @ModelView.button
    @Workflow.transition('admitted')
    def admit(cls, records):
        for record in records:
            record.set_number()
            record.admission()
            record.get_antecedents()

    @classmethod
    @ModelView.button
    @Workflow.transition('diagnosis')
    def diagnosis(cls, records):
        for record in records:
            record.set_professional()

    @classmethod
    @ModelView.button
    @Workflow.transition('discharged')
    def discharge(cls, records):
        for rec in records:
            if rec.discharge_destination == 'hospitalization':
                rec.create_hospitalization()
            rec.set_professional_discharged()
            rec.set_antecedents()

            # This charge all medicaments, lab and images in service
            rec.update_service()

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    def admission(self):
        self._create_service()

    @classmethod
    @ModelView.button
    def create_service(cls, records):
        pass

    def _get_line(self, product, qty):
        ServiceLine = Pool().get('health.service.line')
        unit_price = ServiceLine.compute_price(product, self.insurance_plan)
        return {
            'desc': product.template.name,
            'to_invoice': True,
            'product': product.id,
            'unit_price': unit_price,
            'qty': qty,
            'ward': self.ward.id if self.ward else None,
        }

    def update_service(self):
        # After of discharge the service must be updated adding all the
        # resources that patient consumed.
        Service = Pool().get('health.service')
        lines = []
        for med in self.medications:
            lines.append(self._get_line(med.medicament.product, med.qty))
        for lab in self.labs:
            lines.append(self._get_line(lab.test.product, 1))
        for img in self.imaging_requests:
            lines.append(self._get_line(img.test.product, 1))
        for interc in self.interconsultations:
            lines.append(self._get_line(interc.product, 1))
        Service.write([self.health_service], {
            'lines': [('create', lines)]
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_alcohol():
        return 'no'

    @staticmethod
    def default_smoke():
        return 'no'

    @staticmethod
    def default_coffee():
        return 'no'

    @staticmethod
    def default_registration_date():
        return datetime.now()

    @staticmethod
    def default_ward():
        config = Pool().get('health.configuration').get_config()
        if config and config.ward_emergency:
            return config.ward_emergency.id

    @staticmethod
    def default_location():
        config = Pool().get('health.configuration').get_config()
        if config and config.location_emergency_default:
            return config.location_emergency_default.id

    @staticmethod
    def default_institution():
        return Pool().get('health.institution').get_institution()

    def get_service_lines(self):
        config = Pool().get('health.configuration').get_config()
        return [self._get_line(config.product_emergency, 1)]

    def get_service_data(self):
        data = {
            'patient': self.patient.id,
            'institution': self.institution.id,
            'service_date': date.today(),
            'state': 'draft',
            'desc': 'Emergency',
            'origin': str(self),
            'insurance_plan': self.insurance_plan.id if self.insurance_plan else '',
            'lines': [('create', self.get_service_lines())],
        }
        return data

    def _create_service(self):
        Service = Pool().get('health.service')
        data = self.get_service_data()
        ctx = {}
        if self.insurance_plan and self.insurance_plan.price_list:
            ctx = {'price_list': self.insurance_plan.price_list.id}

        with Transaction().set_context(ctx):
            service, = Service.create([data])
        self.health_service = service.id
        self.save()

    def set_professional(self):
        Professional = Pool().get('health.professional')
        self.attending_medical_prof = Professional.get_health_professional()
        self.save()

    def set_professional_discharged(self):
        # Set te professional that discharged the patient.
        Prof = Pool().get('health.professional')
        self.discharge_attending_physician = Prof.get_health_professional()
        self.save()

    def set_undo_discharged(self):
        # Set state back to diagnosis if patient has been discharged.
        self.state = 'diagnosis'
        self.save()

    def set_antecedents(self):
        Patient = Pool().get('health.patient')
        Patient.write([self.patient], {
            'disease_history': self.disease_history,
            'childhood_history': self.childhood_history,
            'adolescence_history': self.adolescence_history,
            'adulthood_history': self.adulthood_history,
            'family_history': self.family_history,
            'common_medication': self.common_medication,
            'blood_transfusion': self.blood_transfusion,
            'coffee': self.coffee,
            'coffee_frequency': self.coffee_frequency,
            'smoke': self.smoke,
            'smoke_frequency': self.smoke_frequency,
            'alcohol': self.alcohol,
            'alcohol_frequency': self.alcohol_frequency,
            'other_habits': self.other_habits,
            'medications_allergy': self.medications_allergy,
            'food_allergy': self.food_allergy,
            'other_allergy': self.other_allergy,
            'surgical_history': self.surgical_history,
            'trauma_history': self.trauma_history,
        })
        self.save()

    def cancel_diagnosis(self):
        # Cancel emergency diagnosis action for doctor.
        self.state = 'cancelled'
        self.save()

    def generate_shipment(self):
        pool = Pool()
        if self.state not in ('diagnosis', 'admitted'):
            return
        Shipment = pool.get('stock.shipment.out')
        Date = pool.get('ir.date')
        to_create = []
        moves = self.get_stock_moves(self.medications)
        if not moves:
            return
        to_create.append({
            'planned_date': Date.today(),
            'company': Transaction().context.get('company'),
            'customer': self.patient.party.id,
            'reference': self.number,
            'warehouse': self.location.id,
            'delivery_address': self.patient.party.addresses[0].id,
            'moves': [('add', moves)],
        })
        if to_create:
            shipments = Shipment.create(to_create)
            # self.write([self], {
            #     'internal_stock_requests': [('add', [shipments[0].id])]
            # })

    def get_stock_moves(self, medications):
        Move = Pool().get('stock.move')
        moves = []
        for medication in medications:
            if medication.status != 'pending':
                continue
            product = medication.medicament.product
            move_info = {}
            move_info['origin'] = str(self)
            move_info['product'] = product.id
            move_info['uom'] = product.default_uom.id
            move_info['quantity'] = medication.qty
            move_info['from_location'] = self.location.output_location.id
            move_info['to_location'] = self.patient.party.customer_location.id
            move_info['unit_price'] = product.list_price
            move_info['state'] = 'draft'
            new_move, = Move.create([move_info])
            medication.stock_move = new_move.id
            medication.status = 'requested'
            moves.append(new_move)
            medication.save()
        return moves

    def get_antecedents(self):
        self.disease_history = self.patient.disease_history
        self.childhood_history = self.patient.childhood_history
        self.adolescence_history = self.patient.adolescence_history
        self.adulthood_history = self.patient.adulthood_history
        self.family_history = self.patient.family_history
        self.common_medication = self.patient.common_medication
        self.blood_transfusion = self.patient.blood_transfusion
        self.coffee = self.patient.coffee
        self.coffee_frequency = self.patient.coffee_frequency
        self.smoke = self.patient.smoke
        self.smoke_frequency = self.patient.smoke_frequency
        self.alcohol = self.patient.alcohol
        self.alcohol_frequency = self.patient.alcohol_frequency
        self.other_habits = self.patient.other_habits
        self.medications_allergy = self.patient.medications_allergy
        self.food_allergy = self.patient.food_allergy
        self.other_allergy = self.patient.other_allergy
        self.surgical_history = self.patient.surgical_history
        self.trauma_history = self.patient.trauma_history
        self.save()

    def create_hospitalization(self):
        Inpatient = Pool().get('health.inpatient')
        now = datetime.now()
        tomorrow = now + timedelta(days=1)
        values = {
            'patient': self.patient.id,
            'admission_kind': 'emergency',
            'emergency': self.id,
            'hospitalization_date': now,
            'discharge_date': tomorrow,
            'state': 'confirmed',
            'institution': self.institution.id,
        }
        Inpatient.create([values])

    @classmethod
    def delete(cls, records):
        for rec in records:
            if rec.number:
                raise UserError(gettext(
                    'health_emergency.msg_delete_emergency'
                ))
        super(Emergency, cls).delete(records)

    @fields.depends('patient')
    def on_change_patient(self, name=None):
        if self.patient and self.patient.party.puid:
            self.puid = self.patient.party.puid

        if self.patient and self.patient.party.insurance:
            self.insurance_plan = self.patient.party.insurance[0].plan.id


class VitalSigns(ModelSQL, ModelView):
    'Vital Signs'
    __name__ = 'health.emergency.vital_signs'

    emergency = fields.Many2One('health.emergency', 'Emergency Code')
    blood_pressure = fields.Char('Blood Pressure (mm/Hg)',
        help='Reference Values: \n'
            'Normal Values: Sistólica: 116+/-12 Diastólica: 70+/-7 \n'
            'Minimal Values: Sistólica: 90 Diastólica: 60 \n'
            'Alert Values: Sistólica: 130 Diastólica: 90 \n'
            'Emergency Values: Sistólica: 160 Diastólica: 110')
    heart_rate = fields.Integer(
        'Heart Rate', help='Heart rate expressed in beats per minute')
    temperature = fields.Float('Temperature', help='Temperature in celcius')
    respiratory_rate = fields.Integer('Respiratory Rate',
        help='Respiratory rate expressed in breaths per minute')
    osat = fields.Integer(
        'Oxygen Saturation',
        help='Arterial oxygen saturation expressed as a percentage')
    heart_rate_fetal = fields.Integer(
        'FCF', help='Heart rate fetal expressed in beats per minute')
    glycemia = fields.Integer(
        'Glycemia', help='levels of the glucose on blood ')
    taking_date = fields.DateTime('Taking Date',
        help='Date of the taking of samples')

    @staticmethod
    def default_taking_date():
        return datetime.now()


class ToxicHabits(ModelSQL, ModelView):
    'Toxic Habits'
    __name__ = 'health.emergency.toxic_habits'

    emergency = fields.Many2One('health.emergency', 'Emergency Code')
    habit = fields.Selection(TOXIC_HABITS, 'Habit', required=True)
    habit_string = habit.translated('habit')
    quantity = fields.Numeric('Quantity', required=True)
    frequency = fields.Char('Frequency', required=True)
    others = fields.Text('Others', required=True)


class NoteList(ModelSQL, ModelView):
    'Note List'
    __name__ = 'health.emergency.note_list'

    emergency = fields.Many2One('health.emergency', 'Emergency Code')
    attending_physician = fields.Many2One('health.professional',
        'Attending Physician', required=True)
    attending_date = fields.DateTime('Attending Date', readonly=True)
    note = fields.Text('Note')

    @classmethod
    def __setup__(cls):
        super(NoteList, cls).__setup__()
        cls._buttons.update({
                'print_evaluation': {},
                })

    @classmethod
    @ModelView.button_action('health_emergency.report_emergency_evaluations_notes')
    def print_evaluation(cls, records):
        pass

    @staticmethod
    def default_attending_date():
        return datetime.now()

    @classmethod
    def default_attending_physician(cls):
        Professional = Pool().get('health.professional')
        cls.attending_physician = Professional.get_health_professional()
        return cls.attending_physician


class EmergencyProcedures(ModelSQL, ModelView):
    'Emergency Procedures'
    __name__ = 'health.emergency.procedures'
    emergency = fields.Many2One('health.emergency', 'Emergency')
    date_procedure = fields.DateTime('Date', required=True)
    procedure = fields.Many2One('health.procedure', 'procedure', required=True)


class MedicTeam(ModelSQL, ModelView):
    'Medic Team'
    __name__ = 'health.emergency.medic_team'

    emergency = fields.Many2One('health.emergency', 'Emergency Code')
    attending_physician = fields.Many2One('health.professional',
        'Attending Physician', required=True)


class MedicalReleaseReport(Report):
    'Medical Release Report'
    __name__ = 'health_emergency.medical_release_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        time_now = datetime.now() - timedelta(hours=5)
        report_context['hour'] = time_now
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class ReferredPatientReport(Report):
    'Referred Patient Report'
    __name__ = 'health_emergency.referred_patient_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(ReferredPatientReport, cls).get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        time_now = datetime.now() - timedelta(hours=5)
        report_context['hour'] = time_now
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class EvaluationsNotes(Report):
    'Evaluations Notes'
    __name__ = 'health_emergency.emergency.evaluations_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(EvaluationsNotes, cls).get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class EmergencyMedicalHistory(Report):
    'Emergency Medical History'
    __name__ = 'health_emergency.emergency.history_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(EmergencyMedicalHistory, cls).get_context(records, header, data)
        user = Pool().get('res.user')(Transaction().user)
        report_context['company'] = user.company
        report_context['user'] = user
        return report_context


class UndoDischarged(Wizard):
    "Undo Discharged"
    __name__ = 'health.emergency.undo_discharged'
    start_state = 'undo_discharged'
    undo_discharged = StateTransition()

    @classmethod
    def __setup__(cls):
        super(UndoDischarged, cls).__setup__()

    def transition_undo_discharged(self):
        Emergency = Pool().get('health.emergency')
        ids = Transaction().context['active_ids']

        for record in Emergency.browse(ids):
            if record.state!='discharged':
                raise UserError(gettext(
                    'health_emergency.msg_emergency_error_patient_not_discharged'
                ))
            if record.state=='discharged':
                record.set_undo_discharged()
        return 'end'


class CancelDiagnosis(Wizard):
    "Undo Discharged"
    __name__ = 'health.emergency.cancel_diagnosis'
    start_state = 'cancel_diagnosis'
    cancel_diagnosis = StateTransition()

    @classmethod
    def __setup__(cls):
        super(CancelDiagnosis, cls).__setup__()

    def transition_cancel_diagnosis(self):
        Emergency = Pool().get('health.emergency')
        ids = Transaction().context['active_ids']

        for record in Emergency.browse(ids):
            if record.state!='diagnosis':
                raise UserError(gettext(
                    'health_emergency.msg_emergency_cancel_error'
                ))
            if record.state=='diagnosis':
                record.cancel_diagnosis()
        return 'end'


class EmergencyMedication(ModelSQL, ModelView):
    'Emergency Medication'
    __name__ = 'health.emergency.medication'
    emergency = fields.Many2One('health.emergency', 'Emergency')
    medicament = fields.Many2One('health.medicament', 'Medicament',
        required=True, help='Prescribed Medicament')
    qty = fields.Integer('Quantity', required=True,
        help='Quantity of units (eg, 2 capsules) of the medicament')
    indication = fields.Many2One('health.pathology', 'Indication',
        help='Choose a disease for this medicament from the disease list. It'
        ' can be an existing disease of the patient or a prophylactic.')
    application_date = fields.Date('Application Date',
        help='Date of application', required=True)
    dose = fields.Float('Dose',
        help='Amount of medication (eg, 250 mg) per dose', required=True)
    dose_unit = fields.Many2One('health.dose.unit', 'Dose Unit',
        required=True, help='Unit of measure for the medication to be taken')
    route = fields.Many2One('health.drug.route', 'Administration Route',
        required=True, help='Drug administration route code.')
    form = fields.Many2One('health.drug.form', 'Form', required=True,
        help='Drug form, such as tablet or gel')
    adverse_reaction = fields.Text('Adverse Reactions',
        help='Side effects or adverse reactions that the patient experienced')
    status = fields.Selection([
        ('pending', 'Pending'),
        ('requested', 'Requested'),
        ('applied', 'Applied'),
        ], 'Status', required=True, readonly=True)
    stock_move = fields.Many2One('stock.move', 'Stock Move',
        help='Stock move inside internal shipment', states={'readonly': True})

    @staticmethod
    def default_status():
        return 'pending'


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def _get_origin(cls):
        return super(Move, cls)._get_origin() + ['health.emergency']


class HealthLabTest(metaclass=PoolMeta):
    __name__ = 'health.lab_test'

    @classmethod
    def _get_origin(cls):
        return super(HealthLabTest, cls)._get_origin() + ['health.emergency']


class ImagingTestRequest(metaclass=PoolMeta):
    __name__ = 'health.imaging.request'

    @classmethod
    def _get_origin(cls):
        return super(ImagingTestRequest, cls)._get_origin() + ['health.emergency']


class Interconsultation(metaclass=PoolMeta):
    __name__ = 'health.interconsultation'

    @classmethod
    def _get_origin(cls):
        return super(Interconsultation, cls)._get_origin() + ['health.emergency']


class Inpatient(metaclass=PoolMeta):
    __name__ = 'health.inpatient'
    emergency = fields.Many2One('health.emergency', 'Emergency', states={
        'readonly': True,
    })


class AdmissionCause(ModelSQL, ModelView):
    'Admission Cause'
    __name__ = 'health.admission_cause'

    name = fields.Char('Admission Cause', select=True)


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'

    @classmethod
    def _get_origin(cls):
        return super(ShipmentInternal, cls)._get_origin() + ['health.emergency']
